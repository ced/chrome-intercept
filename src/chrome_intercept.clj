(ns chrome_intercept
  (:require [http.async.client :as client]
            [org.httpkit.client :as http]
            [clojure.data.json :as json]))
; A script that allows you to intercept HTTP requests, responses, and response bodies
; from a chrome session, using chrome remote debugging functionality.
; https://developer.chrome.com/devtools/docs/protocol/1.1/network
;
; You can connect to a chrome instance, when chrome is run like this:
; /usr/bin/google-chrome --disable-new-avatar-menu --remote-debugging-port=9222 --enable-benchmarking --enable-net-benchmarking

; Keep track of request ID used when communicating with chrome remote debugger
(def request-counter (agent 0))

(defn gen-string [length]
  "Generate a string of a given length"
  (apply str (take length (repeatedly #(rand-nth "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")))))

(defn build-url [{:keys [protocol host port path], :or {protocol "http"}}]
  (str protocol "://" host ":" port "/" path))

(defn res-conv [response]
  "Convert the json body to clojure data"
  (json/read-str (:body response)))

(defn dump-to-file [data {:keys [path prefix] :or {path "/tmp/chrome_intercept" prefix "chrome_intercept"}}]
  "Dump the given data to the given file"
  (let [length 13
        filename (clojure.string/join "/" [path (str prefix (gen-string length))])]
    (println "Saving to" filename)
    ; Create the path, if it doesn't already exist
    (clojure.java.io/make-parents filename)
    (spit filename (with-out-str (pr data)))))

(defn list-tabs [& [host port]]
  "List available tabs on a remote chrome debugging session"
  (let [h (or host "localhost")
        p (or port 9222)]
    (res-conv @(http/get (build-url {:host h :port p :path "json"})))))

(defn chrome-resp-body [ws req-id]
  "Ask for an HTTP request's response body"
  (let [get-response-body (json/write-str {:id @request-counter :method "Network.getResponseBody" :params {:requestId req-id}})]
    (println "Asking for response body of request" req-id)
    (dump-to-file {:req-id req-id :ask-id @request-counter} {:prefix (str "askForRespBody-" req-id "-")})
    (client/send ws :text get-response-body)
    (send request-counter inc)))

(defn on-open [ws]
  "Connect to remote chrome tab, and ask it to send network traffic info"
  (println "Connected to WebSocket.")
  (let [network-enable (json/write-str {:id @request-counter :method "Network.enable"})]
    (println "Sending" network-enable)
    (client/send ws :text network-enable)
    (send request-counter inc)))

(defn on-close [ws code reason]
  (println "Connection to WebSocket closed.\n"
           (format "(Code %s, reason: %s)" code reason)))

(defn on-error [ws e]
  (println "ERROR:" e))

(defn handle-message [ws msg]
  "Take actions when we receive information from the remote chrome instance"
  (let [response (json/read-str msg)]
    ;(println "Received" (clojure.pprint/pprint response))
    (cond
      ; HTTP requests are saved
      (= "Network.requestWillBeSent" (get response "method")) (dump-to-file response {:prefix (str "request-" (get-in response ["params" "requestId"]) "-")})
      ; HTTP request-responses are saved
      (= "Network.responseReceived" (get response "method")) (dump-to-file response {:prefix (str "response-" (get-in response ["params" "requestId"]) "-")})
      ; Ask for HTTP request's response body after we've received all of it.
      (= "Network.loadingFinished" (get response "method")) (chrome-resp-body ws (get-in response ["params" "requestId"]))
      ; HTTP response body is saved
      (not-any? #(nil? (get-in response %)) [["id"] ["result"] ["result" "body"] ["result" "base64Encoded"]]) (dump-to-file response {:prefix (str "responseBody-" (get response "id") "-")})
      :else (println "Misc rx" response))))

(defn -main []
  ; Pick the remote chrome tab to listen to
  (let [chrome-tabs (map #(select-keys % ["title" "url" "webSocketDebuggerUrl"]) (list-tabs))
        tab-titles (vec (map #(get % "title") chrome-tabs))]
    (doall (for [i (range (count tab-titles))]
      (println i "-" (get tab-titles i))))
    (do (println "Which tab would you like to choose?") (flush) (def selection (read-line)))
    (def selected-ws (get (first 
                            (filter
                              #(= (get tab-titles (read-string selection)) (get % "title"))
                              chrome-tabs)) 
                          "webSocketDebuggerUrl"))
    (println "Listening to" (get tab-titles (read-string selection)) "at" selected-ws))
  (let [client (client/create-client)
        ws (client/websocket client
                             selected-ws
                             :open on-open
                             :close on-close
                             :error on-error
                             :text handle-message
                             :byte handle-message)])
    ; Allow the callbacks to do their thing
    (loop [] (recur)))
