(defproject chrome_intercept "0.1.0-SNAPSHOT"
  :description "Intercept chrome traffic via remote debug web-socket, and save as HAL files"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [http.async.client "0.6.1"]
                 [org.clojure/data.json "0.2.6"]
                 [http-kit "2.1.16"]]
  :main ^:skip-aot chrome_intercept
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
