A script that allows you to intercept HTTP requests, responses, and response bodies
from a chrome session, using [chrome remote debugging](https://developer.chrome.com/devtools/docs/protocol/1.1/network) functionality.
(A similar project is Andrea Cardaci's [chrome-har-capturer](https://github.com/cyrus-and/chrome-har-capturer))

You can connect to a chrome instance, when chrome is run like this:
`/usr/bin/google-chrome --disable-new-avatar-menu --remote-debugging-port=9222 --enable-benchmarking --enable-net-benchmarking`

To run the script, install [leiningen](http://leiningen.org/), cd into the chrome-intercept project directory, and issue `lein run` command.
